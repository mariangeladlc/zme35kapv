sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "sap/ui/table/RowAction",
    "sap/ui/table/RowActionItem",
    "sap/ui/table/RowSettings",
    "sap/m/MessageToast",
    "sap/ui/core/routing/History",
  ],
  function (
    BaseController,
    JSONModel,
    formatter,
    mobileLibrary,
    RowAction,
    RowActionItem,
    RowSettings,
    MessageToast,
    History
  ) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;

    return BaseController.extend("com.stulz.zme35kapv.controller.DetailItem", {
      onInit: function () {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        var oViewModel = new JSONModel({
          busy: false,
          delay: 0,
          lineItemListTitle: this.getResourceBundle().getText(
            "detailLineItemTableHeading"
          ),
        });

        this.setModel(oViewModel, "detailItemView");

        oRouter
          .getRoute("detailitem")
          .attachPatternMatched(this._onObjectMatched, this);
      },

      _onObjectMatched: function (oEvent) {
        //Bind the Context to Detail View
        //            this.getView().bindElement({
        //               path: "/" + oEvent.getParameter("arguments").Ebeln,
        //              model: "view"
        //         });

        var Ebeln = oEvent.getParameter("arguments").Ebeln;
        var Ebelp = oEvent.getParameter("arguments").Ebelp;
        this.getModel()
          .metadataLoaded()
          .then(
            function () {
              var sObjectPath = this.getModel().createKey("OdaItemSet", {
                Ebeln: Ebeln,
                Ebelp: Ebelp,
              });
              this._bindView("/" + sObjectPath);
            }.bind(this)
          );
      },

      _bindView: function (sObjectPath) {
          console.log(sObjectPath);
        // Set busy indicator during view binding
        var oViewModel = this.getModel("detailItemView");
        console.log(oViewModel);

        // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
        oViewModel.setProperty("/busy", false);

        this.getView().bindElement({
          path: sObjectPath,
          events: {
            change: this._onBindingChange.bind(this),
            dataRequested: function () {
              oViewModel.setProperty("/busy", true);
            },
            dataReceived: function () {
              oViewModel.setProperty("/busy", false);
            },
          },
        });
      },

      onNavBack: function () {
        history.go(-1);
      },

      _onBindingChange: function () {
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();
        console.log(oElementBinding);
        // No data for the binding
        if (!oElementBinding.getBoundContext()) {
          this.getRouter().getTargets().display("detailObjectNotFound");
          // if object could not be found, the selection in the master list
          // does not make sense anymore.
          this.getOwnerComponent().oListSelector.clearMasterListSelection();
          return;
        }

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          sObjectId = oObject.Ebeln,
          sObjectName = oObject.Ebeln,
          oViewModel = this.getModel("detailItemView");
        console.log(oObject);
        this.getOwnerComponent().oListSelector.selectAListItem(sPath);

        oViewModel.setProperty(
          "/shareSendEmailSubject",
          oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId])
        );
        oViewModel.setProperty(
          "/shareSendEmailMessage",
          oResourceBundle.getText("shareSendEmailObjectMessage", [
            sObjectName,
            sObjectId,
            location.href,
          ])
        );




      },

    });
  }
);
