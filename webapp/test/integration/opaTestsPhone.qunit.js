/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function() {
	"use strict";

	sap.ui.require([
		"com/stulz/zme35kapv/test/integration/PhoneJourneys"
	], function() {
		QUnit.start();
	});
});